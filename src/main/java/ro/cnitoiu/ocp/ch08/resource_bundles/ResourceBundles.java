package ro.cnitoiu.ocp.ch08.resource_bundles;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by cristian on 11.10.2015.
 */
public class ResourceBundles {

    public static void main(String[] args) {

        runExample("en");
        runExample("fr");

        testJavaResourceBundle("en");

        testDefaultLocale();

        testRBInheritance();
    }


    private static void runExample(String languageCode){

        Locale locale = new Locale(languageCode);
        ResourceBundle rb = ResourceBundle.getBundle("Labels", locale);

        System.out.println(rb.getString("hello"));
    }

    private static void testJavaResourceBundle(String languageCode){

        Locale locale = new Locale(languageCode, "CA");
        ResourceBundle rb = ResourceBundle.getBundle("ro.cnitoiu.ocp.ch08.resource_bundles.Labels", locale);

        System.out.println(rb.getObject("hello"));
    }

    private static void testDefaultLocale(){
        //store locale so can put it back at end
        Locale initial = Locale.getDefault();
        System.out.println("\n" + initial);

        //set locale to Germany
        Locale.setDefault(Locale.GERMANY);
        System.out.println(Locale.getDefault());

        //put original locale back
        Locale.setDefault(initial);
        System.out.println(Locale.getDefault());
    }

    private static void testRBInheritance(){
        Locale locale = new Locale("en", "UK");
        ResourceBundle rb = ResourceBundle.getBundle("RB", locale);

        System.out.println(rb.getString("ride.in") + " " + rb.getString("elevator"));
    }
}

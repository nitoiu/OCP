package ro.cnitoiu.ocp.ch08.resource_bundles;

import java.util.ListResourceBundle;

/**
 * Created by cristian.nitoiu on 14.10.2015.
 */
public class Labels_en_CA extends ListResourceBundle {

    @Override
    protected Object[][] getContents() {
        return new Object[][] {
            { "hello", new StringBuilder("from Java")}
        };
    }
}

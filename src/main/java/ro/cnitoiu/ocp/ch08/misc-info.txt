-> Locale Constructors: The language argument represents an ISO 639 Language code

-> Can't change the locale of existing DateFormat/NumberFormat instances

-> In general, a regex search runs from left to right, and once a source's character
   has been used in a match, it cannot be reused.

-> exam metacharacters:
\d (0-9)
\D (anything but 0-9)
\s (whitespace chars: space, \t, \n, \f, \r)
\S (non-whitespace)
\w (word char: a-z, A-Z, 0-9, "_")
\W (non-word char)
\b (word boundary)  -In these cases, regex is looking for a specific
\B (non-word boundary)      relationship between two adjacent characters.

-> non-exam range attributes
^ (carat) negates the chars specified
nested brackets for union of sets
&& intersection of sets

-> \b = backslash

> The Scanner class has nextXxx() (for instance, nextLong() ) and hasNextXxx()
  (for instance, hasNextDouble() ) methods for every primitive type except char

-> %[arg_index$][flags][width][.precision]conversion char
   arg_index: 1$
   flags:
      - left-justify
      + include a sign
      0 pad with zeroes
      , locale-specific grouping separator (ex. 123,456)
      ( enclose negative numbers in parantheses
   width: minimum number of chars to print
   precision: number of digits to print after the decimal point
   conversion char:
      b boolean
      c char
      d integer
      f floating point
      s string

-> Java chooses the most specific resource bundle it can while giving preference to Java ListResourceBundle.
   a. exactly what we asked for RB_fr_CA
   b. couldn't find exactly what we asked for, now trying just requested language
   c. couldn't find French, now trying default Locale
   d. couldn't find full default Locale country, now trying default Locale language
   e. couldn't find anything any matching Locale, now trying default bundle (RB.java/RB.properties)

-> ListResourceBundle and PropertyResourcesBundle do not share an inheritance hierarchy.
   The default locale's resource bundles do not share a hierarchy with the requested locale's resouce bundles.

 -> When getting a key from a resource bundle, the key must be a string.
    The returned result must be a string or an object
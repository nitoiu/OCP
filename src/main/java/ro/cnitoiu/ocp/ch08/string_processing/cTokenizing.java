package ro.cnitoiu.ocp.ch08.string_processing;

import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Created by cristian on 03.10.2015.
 */
public class cTokenizing {
    public static void main(String[] args) {
        searchWithScanner("\\d\\d", "1b2c335f456");

        tokenizeWithStringSplit("\\d", "ab5 ccc 45 @");
        tokenizeWithScanner("1 true 34 hi");
        tokenizeWithStringTokenizerExample();
    }

    public static void searchWithScanner(String regex, String input){

        System.out.println("input: " + input);
        System.out.println("regex: " + regex);

        try {
            Scanner s = new Scanner(input);
            String token;
            do {
                token = s.findInLine(regex);
                System.out.println("found " + token);
            } while (token != null);
        } catch (Exception e) {
            System.out.println("scan exc");
        }
        System.out.println();
    }

    public static void tokenizeWithStringSplit(String delimiter, String source){
        System.out.println("source: " + source);
        System.out.println("delimiter: " + delimiter);

        String[] tokens = source.split(delimiter);
        System.out.println("count " + tokens.length);

        for(String s : tokens) {
            System.out.println(">" + s + "<");
        }
        System.out.println();
    }

    //Scanners can be constructed using files, streams, or strings as a source.
    public static void tokenizeWithScanner(String input){
        boolean b2, b;
        int i;
        String s, hits = " ";


        //Implicit equivalent call of useDelimiter(" ")
        Scanner s1 = new Scanner(input);
        Scanner s2 = new Scanner(input);

        while(b = s1.hasNext()) {
            s = s1.next(); hits += "s";
        }

        while(b = s2.hasNext()) {
            if (s2.hasNextInt()) {
                i = s2.nextInt();
                hits += "i";
            } else if (s2.hasNextBoolean()) {
                b2 = s2.nextBoolean();
                hits += "b";
            } else {
                s2.next();
                hits += "s2";
            }
        }
        System.out.println("hits " + hits + "\n");
    }

    /*
    - The usage of StringTokenizer is not recommended.
    - StringTokenizer objects use whitespace characters by default as delimiters,
        but they can be constructed with a custom set of delimiters (which are listed as a string).
     */
    public static void tokenizeWithStringTokenizerExample(){
        StringTokenizer st = new StringTokenizer("a bc d e");

        System.out.println("\nString: \"a bc d e\"\n" + st.countTokens() + " tokens");
        while(st.hasMoreTokens()) {
            System.out.print(">" + st.nextToken() + "< ");
        }

        System.out.println("\n " + st.countTokens());

        // Second argument "a" is this StringTokenizer's delimiter
        StringTokenizer st2 = new StringTokenizer("a b cab a ba d", "a");
        System.out.println("\nString: \"a b cab a ba d\"\n" + st2.countTokens() + " tokens");

        while(st2.hasMoreTokens()) {
            System.out.print(">" + st2.nextToken() + "< ");
        }
        System.out.println("\n " + st2.countTokens());
    }
}

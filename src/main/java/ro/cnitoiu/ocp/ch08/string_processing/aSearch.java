package ro.cnitoiu.ocp.ch08.string_processing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by cristian.nitoiu on 30.09.2015.
 */
public class aSearch {
    public static void main(String [] args) {

        reg("ab", "abaaaba");
        reg("aba", "abababa");
        reg("\\d", "a12c3e456f");
        reg("\\w", "a 1 56 _Z");
        reg("\\S", "w1w w$ &#w1");
        reg("\\b", "w2w w$ &#w2");
        reg("\\b", "#ab de#");
        reg("\\B", "#ab de#");

        reg("[a-cA-C]", "cafeBABE");
        reg("0[xX][0-9a-fA-F]", "12 0x 0x12 0Xf 0xg"); //one digit hex numbers

    }

    public static void reg(String regex, String source){
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(source);

        System.out.println("source: " + source);
        System.out.println(" index: 01234567890123456");
        System.out.println("expression: " + m.pattern());

        System.out.print("match positions: ");
        while(m.find()) {
            System.out.print(m.start() + " ");
        }
        System.out.println("\n");
    }

}

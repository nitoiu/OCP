package ro.cnitoiu.ocp.ch08.string_processing;

/**
 * Created by cristian on 04.10.2015.
 */
public class dFormatting {

    public static void main(String[] args) {
        //%[arg_index$][flags][width][.precision]conversion char

        int i1 = -123;
        int i2 = 12345;
        System.out.printf(">%1$(7d< \n", i1);
        System.out.printf(">%0,7d< \n", i2);
        System.out.format(">%+-7d< \n", i2);
        System.out.printf(">%2$b + %1$5d< \n", i1, false);
    }
}

package ro.cnitoiu.ocp.ch08.string_processing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by cristian.nitoiu on 30.09.2015.
 */
public class bSearchGroups {

    public static void main(String[] args) {

        groupReg("\\d+", "1 a12 234b");
        groupReg("0[xX]([0-9a-fA-F])+", "12 0x 0x12 0Xf 0xg"); //all hex numbers

        groupReg("proj1([^,])*", "proj3.txt,proj1sched.pdf,proj1,proj2,proj1.java");
        groupReg("[\\w]+[\\.](txt|pdf|java)", "proj3.txt,proj1sched.pdf,proj1,proj2,proj1.java");

        groupReg("\\d{3}[ -]?\\d{4}", "1234567 gibberish123 4567gibberish 123-4567");
        groupReg("a.c", "ac abc a c");

        /*Greedy quantifiers are considered "greedy" because they force the matcher to read in,
        or eat, the entire input string prior to attempting the first match. */
        groupReg(".*xx", "yyxxxyxx"); //greedy
        /* They start at the beginning of the input string,
            then reluctantly eat one character at a time looking for a match*/
        groupReg(".*?xx", "yyxxxyxx"); //reluctant
        /*the possessive quantifiers always eat the
        entire input string, trying once (and only once) for a match.*/
        groupReg(".*+xx", "yyxxxyxx"); //possessive

        groupRegStartEnd("a?", "aba");        //zero length matches

        findAndReplace("[\\w]+[\\.](txt|pdf|java)", "proj3.txt,proj1sched.pdf,proj1,proj2,proj1.java", "XXX");
    }

    public static void groupReg(String regex, String source){
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(source);

        System.out.println("source: " + source);
        System.out.println(" index: 0123456789012345678");
        System.out.println("pattern: " + m.pattern());

        while(m.find()) {
            System.out.println(m.start() + " " + m.group());
        }
        System.out.println("\n");
    }

    public static void groupRegStartEnd(String regex, String source){
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(source);

        System.out.println("pattern: " + m.pattern());
        while(m.find()){
            System.out.println(m.start() + " - " + m.end() + " - " + m.group());
        }
    }

    public static void findAndReplace(String regex, String source, String replacement){
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(source);

        System.out.println("regex: " + m.pattern());
        System.out.println(source);

        System.out.println(m.replaceAll(replacement));
    }
}

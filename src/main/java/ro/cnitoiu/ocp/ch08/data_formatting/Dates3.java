package ro.cnitoiu.ocp.ch08.data_formatting;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by cristian.nitoiu on 30.09.2015.
 */
public class Dates3 {

    public static void main(String[] args) {
        Date d1 = new Date(1_000_000_000_000L);

        //format method
        DateFormat[] dfa = new DateFormat[6];
        dfa[0] = DateFormat.getInstance();
        dfa[1] = DateFormat.getDateInstance();
        dfa[2] = DateFormat.getDateInstance(DateFormat.SHORT);
        dfa[3] = DateFormat.getDateInstance(DateFormat.MEDIUM);
        dfa[4] = DateFormat.getDateInstance(DateFormat.LONG);
        dfa[5] = DateFormat.getDateInstance(DateFormat.FULL);

        for(DateFormat df : dfa) {
            System.out.println(df.format(d1));
        }



        System.out.println("\n\n");



        //parse method
        Date d2 = new Date(1000000000000L);
        System.out.println("d2:\n\t" + d2.toString());

        DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
        String s = df.format(d2);
        System.out.println(s);

        try {
            Date d3 = df.parse(s);
            System.out.println("parsed:\n\t" + d3.toString());
        } catch (ParseException pe) {
            System.out.println("parse exc"); }
    }

}

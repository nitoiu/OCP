package ro.cnitoiu.ocp.ch08.data_formatting;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by cristian.nitoiu on 30.09.2015.
 */
public class Locales {

    public static void main(String[] args) {

        Calendar c = Calendar.getInstance();
        c.set(2010, 11, 14);

        Date d = c.getTime();

        Locale locIT = new Locale("it", "IT");  //Italy
        Locale locPT = new Locale("pt");        //Portugal
        Locale locBR = new Locale("pt", "BR");  //Brazil
        Locale locIN = new Locale("hi", "IN");  //India
        Locale locJA = new Locale("ja");        //Japan

        DateFormat dfUS = DateFormat.getInstance();
        System.out.println("US\t\t\t" + dfUS.format(d));

        DateFormat dfUSfull = DateFormat.getDateInstance(DateFormat.FULL);
        System.out.println("US full\t\t" + dfUSfull.format(d));

        DateFormat dfIT = DateFormat.getDateInstance(DateFormat.FULL, locIT);
        System.out.println("Italy\t\t" + dfIT.format(d));

        DateFormat dfPT = DateFormat.getDateInstance(DateFormat.FULL, locPT);
        System.out.println("Portugal\t" + dfPT.format(d));

        DateFormat dfBR = DateFormat.getDateInstance(DateFormat.FULL, locBR);
        System.out.println("Brasil\t\t" + dfBR.format(d));

        DateFormat dfIN = DateFormat.getDateInstance(DateFormat.FULL, locIN);
        System.out.println("India\t\t" + dfIN.format(d));

        DateFormat dfJA = DateFormat.getDateInstance(DateFormat.FULL, locJA);
        System.out.println("Japan\t\t" + dfJA.format(d));

        System.out.println("\n\n");

        System.out.println("def " + locBR.getDisplayCountry());
        System.out.println("loc " + locBR.getDisplayCountry(locBR));

        System.out.println("def " + locIT.getDisplayLanguage());
        System.out.println("loc " + locIT.getDisplayLanguage(locIT));
        System.out.println("IT>FR " + locIT.getDisplayLanguage(Locale.FRANCE));

    }
}

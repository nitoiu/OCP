package ro.cnitoiu.ocp.ch08.data_formatting;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Created by cristian.nitoiu on 30.09.2015.
 */
public class NumberFormatClass {

    public static void main(String[] args) {
        float f1 = 123.4567f;
        Locale locFR = new Locale("fr");// France

        NumberFormat[] nfa = new NumberFormat[4];
        nfa[0] = NumberFormat.getInstance();
        nfa[1] = NumberFormat.getInstance(locFR);
        nfa[2] = NumberFormat.getCurrencyInstance();
        nfa[3] = NumberFormat.getCurrencyInstance(locFR);

        for(NumberFormat nf : nfa) {
            System.out.println(nf.format(f1));
        }

        System.out.println("\n\n");

        f1 = 123.45678f;
        NumberFormat nf = NumberFormat.getInstance();
        System.out.println("max fractional digits:" + nf.getMaximumFractionDigits());
        System.out.println("\t" + nf.format(f1));

        nf.setMaximumFractionDigits(5);
        System.out.println("5 fraction digits:\n\t" + nf.format(f1));

        try {
            System.out.println(nf.parse("1234.567"));
            nf.setParseIntegerOnly(true);
            System.out.println("integer only:\n\t" + nf.parse("1234.567"));
        } catch (ParseException pe) {
            System.out.println("parse exc");
        }
    }
}

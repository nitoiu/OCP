package ro.cnitoiu.ocp.ch08.data_formatting;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by cristian on 25.09.2015.
 */
class Dates2 {
    public static void main(String[] args) {
        Date d1 = new Date(1_000_000_000_000L);

        System.out.println(d1.toString());

        Calendar c = Calendar.getInstance();
        c.setTime(d1);

        if (Calendar.SUNDAY == c.getFirstDayOfWeek()){
            System.out.println("Sunday is the first day of the week");
        }


        System.out.println("trillionth milli day of week is "
                + c.get(Calendar.DAY_OF_WEEK));

        c.add(Calendar.MONTH, 1);

        Date d2 = c.getTime();
        System.out.println("c.add(Calendar.MONTH, 1)\n\t" + d2.toString());

        c.roll(Calendar.MONTH, 8);
        System.out.println("c.roll(Calendar.MONTH, 8):\n\t" + c.getTime().toString());
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.cnitoiu.ocp.ch08.data_formatting;

import java.util.Date;

/**
 *
 * @author cristian
 */
public class Dates1 {

    public static void main(String[] args) {
        Date d1 = new Date(1_000_000_000_000L); // a trillion, Java 7 style
        System.out.println("1st date " + d1.toString());
        
        d1.setTime(d1.getTime() + 3_600_000); // 3_600_000 millis / hour
        System.out.println("new time " + d1.toString());
        
        Date now = new Date();
        System.out.println("now time " + now);
        System.out.println(now.getTime());
    }
}

package ro.cnitoiu.ocp.ch09.a_file_navigation_and_IO;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by cristian on 17.10.2015.
 */
public class cDirectories {

    public static void main(String[] args) {

        try {
//            createDir();
//            deleteAndRename();
            searchFiles();

        } catch (Exception e){
            e.printStackTrace();
        }

    }

    private static void createDir() throws IOException{
        File myDir = new File("mydir");
        myDir.mkdir();

        File myFile = new File(myDir, "myfile.txt");
        myFile.createNewFile(); //exception if myDir doesn't exist on disk (not calling mkdir())

        PrintWriter pw = new PrintWriter(myFile);
        pw.println("new stuff");
        pw.flush();
        pw.close();
    }

    private static void deleteAndRename() throws IOException{

        File delDir = new File("deldir");
        delDir.mkdir();

        File delFile1 = new File(delDir, "delfile1.txt");
        delFile1.createNewFile();

        File delFile2 = new File(delDir, "delfile2.txt");
        delFile2.createNewFile();

        delFile1.delete();

        //You can't delete a directory if it's not empty
        System.out.println("deldir is " + delDir.delete()); //attempt to delete de directory

        File newName = new File(delDir, "newName.txt");
        delFile2.renameTo(newName);

        File newDir = new File("newdir");
        delDir.renameTo(newDir);

    }

    private static void searchFiles(){

        String[] files = new File(".").list();

        for(String s : files){
            System.out.println("found " + s);
        }

    }
}

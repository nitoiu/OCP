package ro.cnitoiu.ocp.ch09.a_file_navigation_and_IO;

import java.io.*;

/**
 * Created by cristian on 17.10.2015.
 */
public class bWriter2 {

    public static void main(String[] args) {

//        thePainfulWay();

        usingPrintWriter();

    }

    private static void thePainfulWay(){
        char[] in = new char[50];
        int size = 0;

        try{

            File file = new File("fileWrite2.txt"); //creates object
            FileWriter fw = new FileWriter(file); //creates file

            fw.write("howdy\nfolks\n");

            fw.flush();
            fw.close();

            FileReader fr = new FileReader(file);

            size = fr.read(in);
            System.out.print(size + " ");

            for(int i =0; i < size; ++i){
                System.out.print(in[i]);
            }

            fr.close();

        } catch (IOException e) { }
    }

    private static void usingPrintWriter(){
        //java 1.4 example
        try {
            File file = new File("fileWrite2.txt");
            FileWriter fw = new FileWriter(file);

            PrintWriter pw = new PrintWriter(fw); // create a PrintWriter

            pw.println("howdy");
            pw.println("folks");

            pw.flush();
            pw.close();

            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);

            String data;

            while( (data = br.readLine()) != null){
                System.out.println(data);
            }

            br.close();

        } catch (IOException e) {}
    }
}

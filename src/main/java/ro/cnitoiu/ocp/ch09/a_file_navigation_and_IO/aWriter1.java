package ro.cnitoiu.ocp.ch09.a_file_navigation_and_IO;

import java.io.File;
import java.io.IOException;

/**
 * Created by cristian on 17.10.2015.
 */
class aWriter1 {
    public static void main(String [] args) {

        /*exists(), createNewFile()*/
        try {
            boolean newFile = false;

            File file = new File("fileWrite1.txt"); //project root (where lies the pom.xml)
            System.out.println(file.exists());

            newFile = file.createNewFile();
            System.out.println(newFile);
            System.out.println(file.exists());

        } catch (IOException e) {}
    }
}

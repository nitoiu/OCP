package ro.cnitoiu.ocp.ch09.a_file_navigation_and_IO;

import java.io.Console;

/**
 * Created by cristian on 17.10.2015.
 */
public class dConsole {

    /*to run this goto target/classes folder and use the following command:
    java ro.cnitoiu.ocp.ch09.a_file_navigation_and_IO.dConsole */
    public static void main(String[] args) {
        Console console = System.console();
        char[] pw;

        pw = console.readPassword("%s", "pw: "); //prints pw:, waits for input

        for(char ch: pw)
            console.format("%c ", ch);

        console.format("\n");

        MyUtility mu = new MyUtility();

        while(true) {
            String name = console.readLine("%s", "input?: "); //prints input?: , waits for input
            console.format("output: %s \n", mu.doStuff(name));
        }
    }

    static class MyUtility{

        String doStuff(String arg1){
            //subcode
            return "result is " + arg1;
        }

    }
}

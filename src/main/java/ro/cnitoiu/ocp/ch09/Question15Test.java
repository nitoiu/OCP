package ro.cnitoiu.ocp.ch09;

import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by cristian.nitoiu on 27.10.2015.
 */
public class Question15Test {

    public static void main(String[] args) throws Exception{

        Path zPath = Paths.get("c/x/y/z");
        Files.createDirectories(zPath);

        if(Files.notExists(Paths.get("c/x/a.txt"))) {
            Files.createFile(Paths.get("c/x/a.txt"));
            Files.createFile(Paths.get("c/x/y/b.txt"));
            Files.createFile(Paths.get("c/x/y/z/c.txt"));
        }

        Path dir = Paths.get("c/x");
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "**/*.txt")) {
            for (Path path : stream) {
                System.out.println(path);
            }
        }

    }
}

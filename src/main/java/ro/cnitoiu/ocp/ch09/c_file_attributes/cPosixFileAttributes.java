package ro.cnitoiu.ocp.ch09.c_file_attributes;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.DosFileAttributes;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

/**
 * Created by cristian on 23.10.2015.
 */
public class cPosixFileAttributes {

    public static void main(String[] args) throws Exception{

//        dosExample();
        posixExample();

    }

    private static void dosExample() throws Exception{

        Path path = Paths.get("C:/test");
        Files.createFile(path);

        Files.setAttribute(path, "dos:hidden", true);
        Files.setAttribute(path, "dos:readonly", true);

        DosFileAttributes dos = Files.readAttributes(path, DosFileAttributes.class);

        System.out.println(dos.isHidden());
        System.out.println(dos.isReadOnly());

        Files.setAttribute(path, "dos:hidden", false);
        Files.setAttribute(path, "dos:readonly", false);

        dos = Files.readAttributes(path, DosFileAttributes.class);

        System.out.println(dos.isHidden());
        System.out.println(dos.isReadOnly());
        Files.delete(path);
    }

    private static void posixExample() throws Exception{

        Path path = Paths.get("/tmp/file2");

        if(Files.notExists(path))
            Files.createFile(path);

        PosixFileAttributes posix = Files.readAttributes(path, PosixFileAttributes.class);
        Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rw-r--r--");

        Files.setPosixFilePermissions(path, perms);
        System.out.println(posix.permissions());
        System.out.println(posix.group());
    }
}

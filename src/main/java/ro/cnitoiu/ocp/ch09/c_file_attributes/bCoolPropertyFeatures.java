package ro.cnitoiu.ocp.ch09.c_file_attributes;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

import static ro.cnitoiu.ocp.OCPUtil.*;

/**
 * Created by cristian on 21.10.2015.
 */
public class bCoolPropertyFeatures {

    public static void main(String[] args) throws Exception{

        printPermissions();
        delimit();
        readBasicFileAttributes();
        modifyBasicFileAttributes();

    }

    private static void printPermissions(){
        Path path = Paths.get(".gitignore");

        System.out.println("isExecutable: " + Files.isExecutable(path));
        System.out.println("isReadable: " + Files.isReadable(path));
        System.out.println("isWritable: " + Files.isWritable(path));
    }

    private static void readBasicFileAttributes() throws Exception{
        BasicFileAttributes basic = Files.readAttributes(Paths.get(".gitignore"), BasicFileAttributes.class);

        System.out.println("create: " + basic.creationTime());
        System.out.println("access: " + basic.lastAccessTime());
        System.out.println("modify: " + basic.lastModifiedTime());
        System.out.println("directory: " + basic.isDirectory());
    }

    private static void modifyBasicFileAttributes() throws Exception{
        Path path = Paths.get("progress.txt");
        BasicFileAttributes basic = Files.readAttributes(path, BasicFileAttributes.class);

        FileTime lastUpdated = basic.lastModifiedTime();
        FileTime created = basic.creationTime();
        FileTime now = FileTime.fromMillis(System.currentTimeMillis());

        BasicFileAttributeView basicView = Files.getFileAttributeView(path, BasicFileAttributeView.class);

        basicView.setTimes(lastUpdated, now, created);

    }
}

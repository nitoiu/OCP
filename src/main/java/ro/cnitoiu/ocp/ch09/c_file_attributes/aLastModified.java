package ro.cnitoiu.ocp.ch09.c_file_attributes;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by cristian on 21.10.2015.
 */
public class aLastModified {

    public static void main(String[] args) throws Exception{

        Date januaryFirst = new GregorianCalendar(2013, Calendar.JANUARY, 1).getTime();

        theOldWay(januaryFirst);
        theNewWay(januaryFirst);

    }

    private static void theOldWay(Date date) throws Exception{
        File file = new File("tmp/file");
        file.createNewFile();
        file.setLastModified(date.getTime());

        System.out.println(file.lastModified());

        file.delete();
    }

    private static void theNewWay(Date date) throws Exception{

        Path path = Paths.get("tmp/file");
        Files.createFile(path);

        FileTime fileTime = FileTime.fromMillis(date.getTime());
        Files.setLastModifiedTime(path, fileTime);

        System.out.println(Files.getLastModifiedTime(path));
        Files.delete(path);
    }
}

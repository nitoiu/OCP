package ro.cnitoiu.ocp.ch09.d_filesystem;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by cristian.nitoiu on 26.10.2015.
 */
public class cMyPathMatcher extends SimpleFileVisitor<Path> {

    private PathMatcher matcher = FileSystems.getDefault().
            getPathMatcher("glob:**/password/**.txt");

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {

        if(!Files.isExecutable(dir)){
            return FileVisitResult.SKIP_SUBTREE;
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException{

        if(!Files.isExecutable(file)){
            return FileVisitResult.CONTINUE;
        }

        if(matcher.matches(file)){
            System.out.println(file);
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {

        if(exc instanceof AccessDeniedException){
            return FileVisitResult.CONTINUE;
        }

        return super.visitFileFailed(file, exc);
    }

    public static void main(String[] args) throws Exception{
        cMyPathMatcher mpm = new cMyPathMatcher();

        Files.walkFileTree(Paths.get("/"), mpm);
    }
}

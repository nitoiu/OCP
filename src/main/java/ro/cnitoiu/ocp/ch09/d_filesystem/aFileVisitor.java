package ro.cnitoiu.ocp.ch09.d_filesystem;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import static ro.cnitoiu.ocp.OCPUtil.*;

/**
 * Created by cristian.nitoiu on 23.10.2015.
 */
public class aFileVisitor {

    public static void main(String[] args) throws Exception{

        testDirectoryStream();
        delimit();
        deleteWithFileVisitor();
    }

    private static void testDirectoryStream() throws Exception{
        Path dir = Paths.get("/home");

        DirectoryStream<Path> stream = Files.newDirectoryStream(dir);

        for(Path path : stream){
            System.out.println(path.getFileName());
        }
        stream.close();

        delimit(); //another example, using glob

        String glob = "[ca]*"; //not a regular expression
        stream = Files.newDirectoryStream(dir, glob);
        for(Path path : stream){
            System.out.println(path.getFileName());
        }
    }

    private static void deleteWithFileVisitor() throws Exception{
        RemoveClassFiles rcf = new RemoveClassFiles();

        System.out.println("Deleted files:");
        Files.walkFileTree(Paths.get("target"), rcf);
    }


    static class RemoveClassFiles extends SimpleFileVisitor<Path>{

        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

            if (file.getFileName().toString().endsWith(".class")){
                Files.delete(file);
                System.out.println(file.getFileName());
            }

            return FileVisitResult.CONTINUE;
        }

        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
            System.out.println("pre: " + dir);
            String name = dir.getFileName().toString();

            if (name.equals("c_file_attributes"))
                return FileVisitResult.SKIP_SUBTREE;

            return FileVisitResult.CONTINUE;
        }

        public FileVisitResult visitFileFailed(Path file, IOException exc) {
            return FileVisitResult.CONTINUE;
        }

        public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
            System.out.println("post: " + dir);
            return FileVisitResult.CONTINUE;
        }

    }

}

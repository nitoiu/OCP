package ro.cnitoiu.ocp.ch09.d_filesystem;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;

import static ro.cnitoiu.ocp.OCPUtil.*;

/**
 * Created by cristian.nitoiu on 26.10.2015.
 */
public class bPathMatcher {

    public static void main(String[] args) {

        matchExample();
        globSyntax();
        matchQuestionMark();
        multiplePatternMatch();
        trickyExample();
    }

    private static void matchExample(){
        Path path1 = Paths.get("/home/One.txt");
        Path path2 = Paths.get("One.txt");

        PathMatcher matcher = FileSystems.getDefault().
                getPathMatcher("glob:*.txt");

        System.out.println(matcher.matches(path1));
        System.out.println(matcher.matches(path2));
    }

    private static void matches(Path path, String glob) {
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(glob);

        delimit();
        System.out.println(path + " - " + glob);
        System.out.println(matcher.matches(path));
    }

    private static void globSyntax(){
        Path path = Paths.get("/com/java/One.java");
        matches(path, "glob:*.java");
        matches(path, "glob:**/*.java");
        matches(path, "glob:*");
        matches(path, "glob:**");
    }

    private static void matchQuestionMark(){
        Path path1 = Paths.get("One.java");
        Path path2 = Paths.get("One.ja^a");

        matches(path1, "glob:*.????");
        matches(path1, "glob:*.???");
        matches(path2, "glob:*.????");
        matches(path2, "glob:*.???");
    }

    private static void multiplePatternMatch(){
        Path path1 = Paths.get("Bert-book");
        Path path2 = Paths.get("Kathy-horse");

        /*we can put wildcards inside braces to have multiple glob expressions*/
        matches(path1, "glob:{Bert*,Kathy*}");
        /*we can put common wildcards outside the braces to share them*/
        matches(path2, "glob:{Bert,Kathy}*");
        /*only match the literal strings Bert and Kathy*/
        matches(path1, "glob:{Bert,Kathy}");

    }

    private static void trickyExample(){
        Path path1 = Paths.get("0*b/test/1");
        Path path2 = Paths.get("9\\*b/test/1");
        Path path3 = Paths.get("01b/test/1");
        Path path4 = Paths.get("0*b/1");

        String glob = "glob:[0-9]\\*{A*,b}/**/1";

        matches(path1, glob);
        matches(path2, glob);
        matches(path3, glob);
        matches(path4, glob);
    }
    /*
    [0-9]   One single digit. Any character from 0 to 9
    \\*     Literal asterisk
    {A*,b}  Either a capital A followed by anything or the single char b.
    /**\/   One or more directories with any name
    1       The single character 1.
     */
}

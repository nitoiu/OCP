-> invoking the flush() method guarantees that the last of the data you
    thought you had already written actually gets out to the file

-> Console's readPassword method doesn't return a string; it returns a character array.
    Strings are pooled and it's safer to use character array.

-> The Path interface extends from Iterable<Path>

-> relativize() and resolve() are opposites, relativize only takes a path as argument

-> you cannot delete a read-only file

-> DirectoryStream works as ls UNIX command, works only on 1 dir

-> FileVisitResult.SKIP_SIBLINGS = skip to the end of current folder + SKIP_SUBTREE

-> When using globs: use / instead of \\ so your code behaves more predictably across operating systems

-> Regarding WatchService: two events will trigger for a rename—both ENTRY_CREATE and ENTRY_DELETE
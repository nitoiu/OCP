package ro.cnitoiu.ocp.ch09.b_files_path_paths;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.*;

/**
 * Created by cristian on 17.10.2015.
 */
public class bFilesCreation {

    public static void main(String[] args) {

        try {
            earlierExample();
            createDirs();
            copyMoveDelete();
            copyReplace();

        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private static void earlierExample() throws IOException{
        Path path = Paths.get("fileWrite1.txt");
        System.out.println(Files.exists(path));

        if(!Files.exists(path))
            Files.createFile(path);
        System.out.println(Files.exists(path));

    }

    private static void createDirs() throws IOException{

        Path path = Paths.get("tmp/java/source/directory");
        Path file = Paths.get("tmp/java/source/directory/Program.java");

        Files.createDirectories(path);

        if(Files.notExists(file))
            Files.createFile(file);
    }

    private static void copyMoveDelete() throws IOException{

        Path source = Paths.get("tmp/test11");

        createFileIfNecessary(source);


        Path target = Paths.get("tmp/test22.txt"); //doesn't exist yet

        Files.copy(source, target);
        Files.delete(target);
        Files.move(source, target);

        //Files.delete will throw exception if the file doesn't exist, use Files.deleteIfExists(path) if not sure.
    }

    private static void copyReplace() throws IOException{
        Path one = Paths.get("tmp/test1");
        Path two = Paths.get("tmp/test2.txt");

        createFileIfNecessary(one);
        createFileIfNecessary(two);

        Path target = Paths.get("tmp/test23.txt");

        try{
            Files.copy(one, target);
            Files.copy(two, target);    //throws exception
        } catch (FileAlreadyExistsException e){
            Files.copy(two, target, StandardCopyOption.REPLACE_EXISTING);
        }


    }

    private static void createFileIfNecessary(Path source) throws IOException{
        if(!Files.exists(source)) {
            Files.createDirectories(source.getParent());
            Files.createFile(source);

            PrintWriter pw = new PrintWriter(source.toFile());
            pw.println("Hello, if you copy this text you will die.\nOh well, you will die anyway!");
            pw.flush();
            pw.close();
        }
    }
}

package ro.cnitoiu.ocp.ch09.b_files_path_paths;

import java.io.File;
import java.net.URI;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by cristian on 17.10.2015.
 */
public class aPaths {

    public static void main(String[] args) {

        syntax4GettingPath();
    }

    private static void syntax4GettingPath(){

        Path p1 = Paths.get("/tmp/file1.txt"); //absolute path
        Path p3 = Paths.get("/tmp", "file1.txt"); //same path

        //windows:
//        Path p2 = Paths.get("c:\\temp\\test");
//        Path p4 = Paths.get("c:", "temp", "test");

        Path p = Paths.get(URI.create("file:///tmp/file1.txt"));

        Path longer = FileSystems.getDefault().getPath("/tmp", "file1.txt");

        //converting from File to Path and back
        File convertedFile = p.toFile();
        Path pp = convertedFile.toPath();
    }
}

package ro.cnitoiu.ocp.ch09.b_files_path_paths;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by cristian.nitoiu on 21.10.2015.
 */
public class cPaths2 {

    public static void main(String[] args) {

        Path path = Paths.get("src/main/resources");

        printPathInfo(path);
        printIterablePath(path);
        normalizingPaths();
        resolvingPaths();
        relativizingPaths();

    }

    private static void printPathInfo(Path path){

        System.out.println("getFileName: " + path.getFileName());
        System.out.println("getName(1): " + path.getName(1));
        System.out.println("getNameCount: " + path.getNameCount());
        System.out.println("getParent: " + path.getParent());
        System.out.println("getRoot: " + path.getRoot());
        System.out.println("subpath(0, 2): " + path.subpath(0, 2));
        System.out.println("toString: " + path.toString());
    }

    private static void printIterablePath(Path path){
        int spaces = 1;

        System.out.println("------------");

        for(Path subPath : path){
            System.out.format("%" + spaces + "s%s%n", " ", subPath);
            spaces += 2;
        }
    }

    private static void normalizingPaths(){

        String path1 = "src/main/resources";
        String path2 = "main/java";
        String upTwoDirectories = "../..";

        Path path = Paths.get(path1, upTwoDirectories, path2);

        System.out.println("-------------");
        System.out.println("Original: " + path);
        System.out.println("Normalized: " + path.normalize());

        System.out.println(Paths.get("/a/./b/./c").normalize());
        System.out.println(Paths.get(".classpath").normalize());
        System.out.println(Paths.get("/a/b/c/..").normalize());
        System.out.println(Paths.get("../a/b/c").normalize());

    }

    private static void resolvingPaths(){

        System.out.println("----------------");

        Path dir = Paths.get("/home/java");
        Path file = Paths.get("models/Model.pdf");
        Path result = dir.resolve(file);
        System.out.println("result = " + result);

        Path absolute = Paths.get("/home/java");
        Path relative = Paths.get("dir");
        file = Paths.get("Model.pdf");
        System.out.println("1: " + absolute.resolve(relative));
        System.out.println("2: " + absolute.resolve(file));
        System.out.println("3: " + relative.resolve(file));
        System.out.println("4: " + relative.resolve(absolute)); //bad
        System.out.println("5: " + file.resolve(absolute)); //bad
        System.out.println("6: " + file.resolve(relative)); //bad

//        Path.resolve(null); //Ambiguous Method call
    }

    private static void relativizingPaths(){

        System.out.println("----------------");

        Path dir = Paths.get("/home/java");
        Path music = Paths.get("/home/java/country/Swift.mp3");
        Path mp3 = dir.relativize(music);
        System.out.println("dir path: " + dir);
        System.out.println("music path: " + music);
        System.out.println("relative path: " + mp3);

        Path absolute1 = Paths.get("/home/java");
        Path absolute2 = Paths.get("/usr/local");
        Path absolute3 = Paths.get("/home/java/temp/music.mp3");
        Path relative1 = Paths.get("temp");
        Path relative2 = Paths.get("temp/music.pdf");
        System.out.println("1: " + absolute1.relativize(absolute3));
        System.out.println("2: " + absolute3.relativize(absolute1));
        System.out.println("3: " + absolute1.relativize(absolute2));
        System.out.println("4: " + relative1.relativize(relative2));
        System.out.println("5: " + absolute1.relativize(relative1)); //bad
    }
}

package ro.cnitoiu.ocp.ch09.e_watch_service;

import java.nio.file.*;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Created by cristian.nitoiu on 26.10.2015.
 */
public class aWatchService {

    public static void main(String[] args) throws Exception{
        Path dir = Paths.get("tmp");

        WatchService watcher = FileSystems.getDefault().newWatchService();

        dir.register(watcher, ENTRY_DELETE);
//        dir.register(watcher, ENTRY_DELETE, ENTRY_CREATE, ENTRY_MODIFY);

        while(true) {
            WatchKey key;

            try{
                key = watcher.take(); //wait for deletion
//              key =  watcher.poll(10, TimeUnit.SECONDS); //may also return null
            } catch (InterruptedException x){
                return;
            }

            for(WatchEvent<?> event : key.pollEvents()){

                WatchEvent.Kind<?> kind = event.kind();

                System.out.println(kind.name());
                System.out.println(kind.type());
                System.out.println(event.context());

                String name = event.context().toString();

                if(name.equals("directoryToDelete")){
                    System.out.println("Directory deleted, now we can proceed");
                    return;
                }

            }

            key.reset();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.cnitoiu.ocp.ch07.exceptions;

import java.io.IOException;

/**
 *
 * @author cristian.nitoiu
 */
public class SupressedExceptions {

    public static void main(String[] args) {
        try (Resource one = new Resource()) {
            throw new Exception("Try");
        } catch (Exception e) {
            System.err.println(e.getMessage());
            for (Throwable t : e.getSuppressed()) {
                System.err.println("suppressed:" + t);
            }
        }
    
    }
}

    
class Resource implements AutoCloseable {
    public void close() throws IOException {
        throw new IOException("Closing");
    }
}
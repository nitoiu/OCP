package ro.cnitoiu.ocp.ch07.exceptions;

import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author cristian.nitoiu
 */
public class MultipleCatch {
    
    private void multipleCatch(){
        try {
            couldThrowAnException();
        } catch (SQLException | IOException e) {
            handleErrorCase(e);
        }
    }  
    
    //This is a common pattern called "handle and declare."
    private void rethrow1() throws SQLException, IOException {
        try {
            couldThrowAnException();
        } catch (SQLException | IOException e) {
            System.out.println(e);
            throw e;
        }
    }
    
    private void rethrow2() throws SQLException, IOException {
        try {
            couldThrowAnException();
            } catch (Exception e) {
                // watch out: this isn't really catching all exception subclasses
                System.out.println(e);
                throw e;
                // note: won't compile in Java 6
            }
    }
 
    //compiler error
    private void failRethrow1() throws SQLException, IOException {
        try {
            couldThrowAnException();
        } catch (SQLException | IOException e) {
            //e = new IOException(); //fails here
            throw e;
        }
    }

    //compiler error
    private void failRethrow2() throws SQLException, IOException {
        try {
            couldThrowAnException();
        } catch (Exception e) {
            e = new IOException();
            //throw e; //fails here
        }
    }
    
    private void handleErrorCase(Exception e) {}
    private void couldThrowAnException() throws IOException, SQLException {}
}

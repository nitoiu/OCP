/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.cnitoiu.ocp.ch07.exceptions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

/**
 *
 * @author cristian.nitoiu
 */
class TryWithResourcesExamples {
    
    private void tryWithResourcesTryCatchBlocks() throws IOException{
        File file = new File("some-file");
        
        try (Reader reader = new BufferedReader(new FileReader(file))) {
        } catch (IOException e) { 
            System.out.println(e);
            throw e;
        }
    }
    
    private void tryWithResourcesTryBlock() throws IOException{
        File file = new File("some-file");
        
        try (Reader reader = new BufferedReader(new FileReader(file))) {
        } 
    }
    
    private void tryWithMultipleResources() throws IOException{
        File file = new File("some-file");
        
        try (Reader reader1 = new BufferedReader(new FileReader(file));
             Reader reader2 = new BufferedReader(new FileReader(file))) {
        } 
    }    
}

class MyResource implements AutoCloseable {
    public void close() {
    // take care of closing the resource
    }
}

//The following example is as complicated as try -with-resources gets:
class One implements AutoCloseable {

    public void close() {
        System.out.println("Close - One");
    } 
}

class Two implements AutoCloseable {

    public void close() {
        System.out.println("Close - Two");
    } 
}

public class TryWithResources {

    public static void main(String[] args) {
        try (One one = new One(); 
             Two two = new Two()) {
            
            System.out.println("Try");
            throw new RuntimeException();
        } catch (Exception e) {
            System.out.println("Catch");
        } finally {
            System.out.println("Finally");
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.cnitoiu.ocp.ch07.exceptions;

import java.io.IOException;

/**
 *
 * @author cristian.nitoiu
 */
class Bad implements AutoCloseable {

    String name;
    
    Bad(String n) {
        name = n;
    }

    public void close() throws IOException {
        throw new IOException("Closing - " + name);
    }
}

public class SupressedExceptions2 {

    public static void main(String[] args) {
        try (Bad b1 = new Bad("1"); Bad b2 = new Bad("2")) {
// do stuff
        } catch (Exception e) {
            System.err.println(e.getMessage());
            for (Throwable t : e.getSuppressed()) {
                System.err.println("suppressed:" + t);
            }
        }
    }
}

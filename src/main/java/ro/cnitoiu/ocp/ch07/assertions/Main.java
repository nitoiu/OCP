package ro.cnitoiu.ocp.ch07.assertions;

/**
 *
 * @author cristian.nitoiu
 */
public class Main {
    
    public static void main(String[] args){
        reallySimple();
        simple();
        go();
    }
    
    private static void reallySimple() {
        int y = 2, x = 1;      
        assert (y > x);
    }
    
    private static void simple() {
        int x = 1, y = 2; 
        assert (y > x): "y is " + y + " x is " + x;
    }
    
    private static void go() {
        int x = 1;
        boolean b = true;
        
        // the following six are legal assert statements
        assert(x == 1);
        assert(b);
        assert true;
        assert(x == 1) : x;
        assert(x == 1) : aReturn();
    }
    
    private static int aReturn() {
        return 1;
    }
}

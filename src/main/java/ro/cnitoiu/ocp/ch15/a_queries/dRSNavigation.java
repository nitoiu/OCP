package ro.cnitoiu.ocp.ch15.a_queries;

import ro.cnitoiu.ocp.OCPUtil;
import ro.cnitoiu.ocp.ch15.JDBCUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by cristian.nitoiu on 26.11.2015.
 */
public class dRSNavigation {

    public static void main(String[] args) throws SQLException{

        Statement statement = JDBCUtil.getConnection().
                    createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);

        ResultSet rs = statement.executeQuery("select * from customer;");

        bResultSetMetaData.printDynamically(rs);
        rs.beforeFirst();
        updateRows(rs);

        OCPUtil.delimit();

        rs.beforeFirst();
        bResultSetMetaData.printDynamically(rs);

        System.out.println("\nRow Count:" + getRowCount(rs));
    }

    static void updateRows(ResultSet rs) throws SQLException{

        while (rs.next()){

            boolean shouldUpdate = false;
            String[] emailAddress = rs.getString("email").split("@");

            if(emailAddress[1].equals("yahoo.com")){
                emailAddress[1] = "gmail.com";
                shouldUpdate = true;
            } else if (emailAddress[1].equals("gmail.com")){
                emailAddress[1] = "yahoo.com";
                shouldUpdate = true;
            }

            if(shouldUpdate){
                rs.updateString("email", emailAddress[0] + "@" + emailAddress[1]);
                rs.updateRow();
            }
        }
    }

/* preserves current position of the cursor in the ResultSet */
    static int getRowCount(ResultSet rs) throws SQLException {

        int rowCount = -1;

        if (rs != null) {
            int currRow = rs.getRow();

            if (rs.isAfterLast()) {
                currRow = -1;
            }
            if (rs.last()) {
                rowCount = rs.getRow();
                if (currRow == -1) {
                    rs.afterLast();
                } else if (currRow == 0) {
                    rs.beforeFirst();
                } else {
                    rs.absolute(currRow);
                }
            }
        }
        return rowCount;
    }
}

package ro.cnitoiu.ocp.ch15.a_queries;

import ro.cnitoiu.ocp.ch15.JDBCUtil;

import java.sql.*;

import static java.lang.System.out;

/**
 * Created by cristian.nitoiu on 25.11.2015.
 */
public class bResultSetMetaData {

    public static void main(String[] args) throws SQLException{

        Connection connection = JDBCUtil.getConnection();
        Statement stmt = connection.createStatement();

        ResultSet rs = stmt.executeQuery("select * from customer");

        printDynamically(rs);
    }

    public static void printDynamically(ResultSet rs) throws SQLException{

        ResultSetMetaData rsmd = rs.getMetaData();
        int cols = rsmd.getColumnCount();

        for (int i = 1; i <= cols; i++) {
            System.out.format("%-" + rsmd.getColumnDisplaySize(i) + "s", rsmd.getColumnName(i));
        }
        System.out.println();

        while (rs.next()) {
            for (int i = 1; i <= cols; i++) {
                System.out.format("%-" + rsmd.getColumnDisplaySize(i) + "s", rs.getObject(i));
            }
            out.println();
        }
    }
}

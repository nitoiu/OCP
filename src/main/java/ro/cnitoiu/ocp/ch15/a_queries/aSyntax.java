package ro.cnitoiu.ocp.ch15.a_queries;

import ro.cnitoiu.ocp.ch15.JDBCUtil;

import java.sql.*;

import static java.lang.System.*;

/**
 * Created by cristian on 24.11.2015.
 */
public class aSyntax {
    public static void main(String[] args) throws SQLException{
        workingExample();

    }

    static void syntax(){
        String url = "jdbc:derby://localhost:1521/BookSellerDB";
        String user = "bookguy";
        String pwd = "$3lleR";

        try {
            Connection conn = DriverManager.getConnection(url, user, pwd); // Get Connection
            Statement stmt = conn.createStatement(); // Create Statement

            String query = "SELECT * FROM Customer";
            ResultSet rs = stmt.executeQuery(query); // Execute Query

            while (rs.next()){ // Process Results
                out.print(rs.getInt("CustomerID") + " "); // Print Columns
                out.print(rs.getString("FirstName") + " ");
                out.print(rs.getString("LastName") + " ");
                out.print(rs.getString("EMail") + " ");
                out.println(rs.getString("Phone"));
            }
        } catch (SQLException se) { }
    }

    static void workingExample() throws SQLException{
        Connection connection = JDBCUtil.getConnection();

        Statement stmt = connection.createStatement(); // Create Statement

        String query = "SELECT * FROM Customer";
        ResultSet rs = stmt.executeQuery(query); // Execute Query

        while (rs.next()){ // Process Results
            out.print(rs.getInt("CustomerID") + " "); // Print Columns
            out.print(rs.getString("FirstName") + " "); //also supports retrieval by index: rs.getString(2);
            out.print(rs.getString("LastName") + " ");
            out.print(rs.getString("EMail") + " ");
            out.println(rs.getString("Phone"));
        }
    }
}

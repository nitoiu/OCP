package ro.cnitoiu.ocp.ch15.a_queries;

import ro.cnitoiu.ocp.ch15.JDBCUtil;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import static java.lang.System.*;

/**
 * Created by cristian.nitoiu on 25.11.2015.
 */
public class cDatabaseMetaData {

    public static void main(String[] args) throws SQLException{
        Connection connection = JDBCUtil.getConnection();

        DatabaseMetaData databaseMetaData = connection.getMetaData();

        if (databaseMetaData.supportsResultSetType(ResultSet.TYPE_FORWARD_ONLY)) {
            out.print("Supports TYPE_FORWARD_ONLY");
            if (databaseMetaData.supportsResultSetConcurrency(
                    ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_UPDATABLE)) {
                out.println(" and supports CONCUR_UPDATABLE");
            }
        }

        if (databaseMetaData.supportsResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE)) {
            out.print("Supports TYPE_SCROLL_INSENSITIVE");
            if (databaseMetaData.supportsResultSetConcurrency(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE)) {
                out.println(" and supports CONCUR_UPDATABLE");
            }
        }

        if (databaseMetaData.supportsResultSetType(ResultSet.TYPE_SCROLL_SENSITIVE)) {
            out.print("Supports TYPE_SCROLL_SENSITIVE");
            if (databaseMetaData.supportsResultSetConcurrency(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE)) {
                out.println("Supports CONCUR_UPDATABLE");
            }
        }


    }

}

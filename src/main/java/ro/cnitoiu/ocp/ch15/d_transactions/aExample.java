package ro.cnitoiu.ocp.ch15.d_transactions;

import java.sql.*;

/**
 * Created by cristian.nitoiu on 09.12.2015.
 */
public class aExample {

    public static void main(String[] args) throws SQLException{

    }

    private static void syntaxExample() throws SQLException{
        String url="", username="", password="";
        Savepoint sp1;


        Connection conn = DriverManager.getConnection(url, username, password);
        conn.setAutoCommit(false); // Start a transaction
        Statement stmt = conn.createStatement();
        int result1, result2, result3;
        try {
            result1 = stmt.executeUpdate("INSERT INTO Author VALUES(1031, 'Rachel', 'McGinn')");
            result2 = stmt.executeUpdate("INSERT INTO Book VALUES('0554466789', 'My American Dolls', '2012-08-31','Paperback', 7.95)");

            sp1 = conn.setSavepoint();
        } catch (SQLException ex) {
            conn.rollback(); // Rollback the entire transaction if an exception thrown, explicitly

            throw ex;
        }

        try {

            result3 = stmt.executeUpdate("INSERT INTO Books_by_Author VALUES(1031,'0554466789')");

            conn.commit(); // No exception: commit the entire transaction

        } catch (SQLException ex) {
            conn.rollback(sp1); // Rollback to savepoint

            conn.commit();
        }
    }

}

package ro.cnitoiu.ocp.ch15;

import java.sql.*;

/**
 * Created by cristian on 24.11.2015.
 */
public class JDBCUtil {

    private static Connection connection;

    public static Connection getConnection(){

        try {
            connection = DriverManager.getConnection("jdbc:h2:~/ocp", "cnitoiu", "");

            initializeDB();

            return connection;
        } catch (SQLException e){
            e.printStackTrace();
        }

        return null;
    }

    static void initializeDB(){
        createAndPopulateCustomer();
    }

    static void createAndPopulateCustomer(){

        try {

            if (doesTableExist("CUSTOMER"))
                return;

            Statement stmt = connection.createStatement();

            String sql = "CREATE TABLE CUSTOMER " +
                    "(CustomerID INTEGER not NULL, " +
                    "FirstName VARCHAR(50), " +
                    "LastName VARCHAR(50), " +
                    "EMail VARCHAR(50), " +
                    "Phone VARCHAR(50), " +
                    " PRIMARY KEY ( FirstName ))";

            stmt.executeUpdate(sql);

            Statement insertStatement = connection.createStatement();

            insertStatement.addBatch("INSERT INTO Customer values (5000, 'John', 'Smith', 'John.Smith@comcast.net', '555-340-1230');");
            insertStatement.addBatch("INSERT INTO Customer values (5001, 'Mary', 'Johnson', 'mary.johnson@comcast.net', '555-123-4567');");
            insertStatement.addBatch("INSERT INTO Customer values (5002, 'Bob', 'Collins', 'bob.collins@yahoo.com', '555-012-3456');");
            insertStatement.addBatch("INSERT INTO Customer values (5003, 'Rebecca', 'Mayer', 'rebecca.mayer@gmail.com', '555-205-8212');");
            insertStatement.addBatch("INSERT INTO Customer values (5006, 'Anthony', 'Clark', 'Janthony.clark@gmail.com', '555-256-1901');");
            insertStatement.addBatch("INSERT INTO Customer values (5007, 'Judy', 'Sousa', 'judy.sousa@verizon.net', '555-256-1901');");
            insertStatement.addBatch("INSERT INTO Customer values (5008, 'Christopher', 'Patriquin', 'patriquinc@yahoo.com', '555-316-1803');");
            insertStatement.addBatch("INSERT INTO Customer values (5009, 'Deborah', 'Smith', 'debsmith@comcast.net', '555-256-3421');");
            insertStatement.addBatch("INSERT INTO Customer values (5010, 'Jennifer', 'McGinn', 'jmcginn@comcast.net', '555-250-0918');");

            int[] updateCounts = insertStatement.executeBatch();

            connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    static boolean doesTableExist(String tableName) throws SQLException{

        DatabaseMetaData dbm = connection.getMetaData();

        ResultSet tables = dbm.getTables(null, null, tableName, null);

        if (tables.next()){
            return true;
        }

        return false;
    }
}

/*
Database URLs

Embedded
jdbc:h2:~/test 'test' in the user home directory
jdbc:h2:/data/test 'test' in the directory /data
jdbc:h2:test in the current(!) working directory

In-Memory
jdbc:h2:mem:test multiple connections in one process
jdbc:h2:mem: unnamed private; one connection

Server Mode
jdbc:h2:tcp://localhost/~/test user home dir
jdbc:h2:tcp://localhost//data/test absolute dir
Server start:java -cp *.jar org.h2.tools.Server

Settings
jdbc:h2:..;MODE=MySQL compatibility (or HSQLDB,...)
jdbc:h2:..;TRACE_LEVEL_FILE=3 log to *.trace.db
 */

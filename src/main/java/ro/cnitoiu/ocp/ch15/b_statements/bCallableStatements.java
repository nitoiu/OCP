package ro.cnitoiu.ocp.ch15.b_statements;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

/**
 * Created by cristian.nitoiu on 07.12.2015.
 */
public class bCallableStatements {

    public static void main(String[] args){
    }

    private static void syntaxExample1() throws Exception{
        int customerID = 5001;
        java.sql.Date fromDate = null; // The start date for the search
        java.sql.Date toDate = null; // The end date for the search
        Connection conn = null;

        String getBooksInDateRange = "{call getBooksDateRange(?, ?, ?)}";
        CallableStatement cstmt = conn.prepareCall(getBooksInDateRange,
                                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                                    ResultSet.CONCUR_UPDATABLE);

        cstmt.setInt(1, customerID); // IN parameter 1 for customerID
        cstmt.setDate(2, fromDate); // IN parameter 2 for fromDate
        cstmt.setDate(3, toDate); // IN parameter 3 for toDate

        ResultSet rs = cstmt.executeQuery();
    }

    private static void syntaxExample2() throws Exception{
        int customerID = 5001;
        double customerTotal;
        Connection conn = null;

        CallableStatement cstmt = conn.prepareCall("{? = call customerTotal (?)}");
        cstmt.registerOutParameter(1, java.sql.Types.DOUBLE);           // register
                                                                        // the OUT
                                                                        // parameter
        cstmt.setInt(2, customerID);
        cstmt.execute();    // Note we are not returning a ResultSet,
                            // so execute is the appropriate method

        customerTotal = cstmt.getDouble(1);
    }

    private static void syntaxExample3() throws Exception{
        int customerID = 5001;
        int numberOfOrders;
        Connection conn = null;

        CallableStatement cstmt = conn.prepareCall("{call customerOrderCount (?)}"); // INOUT

        cstmt.setInt(1, customerID); // set the IN part of the parameter
        cstmt.registerOutParameter(1, java.sql.Types.INTEGER); // the OUT part

        cstmt.execute();
        numberOfOrders = cstmt.getInt(1);
    }
}

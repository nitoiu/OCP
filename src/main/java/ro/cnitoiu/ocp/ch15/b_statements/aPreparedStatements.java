package ro.cnitoiu.ocp.ch15.b_statements;

import ro.cnitoiu.ocp.ch15.JDBCUtil;
import ro.cnitoiu.ocp.ch15.a_queries.bResultSetMetaData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by cristian on 07.12.2015.
 */
public class aPreparedStatements {

    public static void main(String[] args) {
        example();
    }

    static void example(){
        try {
            Connection connection = JDBCUtil.getConnection();

            String pQuery = "SELECT FIRSTNAME, PHONE from CUSTOMER WHERE EMAIL LIKE ?";
            PreparedStatement pstmt = connection.prepareStatement(pQuery);

            pstmt.setString(1, "%comcast%"); // Substitute this String for the

            ResultSet rs = pstmt.executeQuery();

            bResultSetMetaData.printDynamically(rs);

        } catch (SQLException se) {

        }
    }
}

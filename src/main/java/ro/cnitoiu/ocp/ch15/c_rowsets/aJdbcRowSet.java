package ro.cnitoiu.ocp.ch15.c_rowsets;

import org.apache.commons.lang.RandomStringUtils;
import ro.cnitoiu.ocp.ch15.a_queries.bResultSetMetaData;

import javax.sql.RowSet;
import javax.sql.RowSetEvent;
import javax.sql.RowSetListener;
import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;
import java.sql.SQLException;
import java.util.Random;

/**
 * Created by cristian.nitoiu on 08.12.2015.
 *
 * The JdbcRowSet object takes care of creating the connection, creating a statement, and executing the query.
 */
public class aJdbcRowSet {

    public static void main(String[] args) {
        example();
    }

    private static void example(){
        String url = "jdbc:h2:~/ocp";
        String user = "cnitoiu";
        String pwd = "";

        // Construct a JdbcRowSet object in a try-with-resources statement
        try (JdbcRowSet jrs = RowSetProvider.newFactory().createJdbcRowSet()) {
            String query = "SELECT * FROM Customer";
            jrs.setCommand(query);

            // Set the query to build the RowSet
            jrs.setUrl(url);        // JDBC URL
            jrs.setUsername(user);  // JDBC username
            jrs.setPassword(pwd);   // JDBC password

            //add a listener to the rowset
            jrs.addRowSetListener(new MyRowSetListener());

            //Once the execute statement completes, we have a connected JdbcRowSet
            jrs.execute();      // Execute the query stored in setCommand


            //Insert a new Row
            jrs.moveToInsertRow();
            jrs.updateInt("CustomerID", new Random().nextInt(10000));
            jrs.updateString("FirstName", RandomStringUtils.randomAlphabetic(10));
            jrs.updateString("LastName", RandomStringUtils.randomAlphabetic(10));
            jrs.updateString("EMail", RandomStringUtils.randomAlphabetic(6) + "@" + "inserted.com");
            jrs.updateString("Phone", RandomStringUtils.randomNumeric(3) + "-" +
                    RandomStringUtils.randomNumeric(3) + "-" +
                    RandomStringUtils.randomNumeric(4));

            //inserting a row has no effect on the current RowSet data.
            jrs.insertRow();
//            //re-execute the query to see the changes, or use a listener
//            jrs.execute();

            jrs.beforeFirst();

            bResultSetMetaData.printDynamically(jrs); //JdbcRowSet is a ResultSet

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

class MyRowSetListener implements RowSetListener {

    @Override
    /*  A row changed: updated, inserted or deleted. */
    public void rowChanged(RowSetEvent event) {

        if (event.getSource() instanceof RowSet) {
            try {
                ((RowSet) event.getSource()).execute(); // Re-execute the query, refreshing the results
            } catch (SQLException se) {
                System.out.println("SQLException during execute");
            }
        }
    }
    @Override
    public void cursorMoved(RowSetEvent event) { // Cursor moved
    }

    @Override
    public void rowSetChanged(RowSetEvent event) { // Entire RowSet changed
    }
}

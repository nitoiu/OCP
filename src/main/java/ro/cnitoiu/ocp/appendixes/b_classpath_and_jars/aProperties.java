package ro.cnitoiu.ocp.appendixes.b_classpath_and_jars;

import java.util.Properties;

/**
 * Created by cristian.nitoiu on 15.12.2015.
 */
public class aProperties {

    public static void main(String[] args) {

        Properties p = System.getProperties();
        p.setProperty("myProp", "myValue");
        p.list(System.out);

    }
}

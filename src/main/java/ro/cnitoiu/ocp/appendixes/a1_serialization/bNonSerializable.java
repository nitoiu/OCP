package ro.cnitoiu.ocp.appendixes.a1_serialization;

import java.io.*;

/**
 * Created by cristian on 14.12.2015.
 */
public class bNonSerializable {

    public static void main(String[] args) {

        Collar2 c = new Collar2(3);
        Dog2 d = new Dog2(c, 5);

        System.out.println("before: collar size is " + d.getCollar().getCollarSize());
        try {
            FileOutputStream fs = new FileOutputStream("testSer.ser");
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(d);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            FileInputStream fis = new FileInputStream("testSer.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            d = (Dog2) ois.readObject();
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("after: collar size is " + d.getCollar().getCollarSize());
    }
}

class Dog2 implements Serializable {
    transient private Collar2 theCollar; //We can't serialize this
    private int dogSize;

    public Dog2(Collar2 collar, int size) {
        theCollar = collar;
        dogSize = size;
    }

    /* Like most I/O-related methods writeObject() can throw exceptions.
    You can declare them or handle them but we recommend handling them. */
    private void writeObject(ObjectOutputStream os) throws IOException{
        os.defaultWriteObject();

//      you have to read the extra stuff in the same order you wrote it.. including before or after defaultWriteObject
        os.writeInt(theCollar.getCollarSize());
    }

    private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException {
        is.defaultReadObject();
        theCollar = new Collar2(is.readInt());
    }


    public Collar2 getCollar() {
        return theCollar;
    }
}

class Collar2 {
    private int collarSize;

    public Collar2(int size) {
        collarSize = size;
    }
    public int getCollarSize() {
        return collarSize;
    }
}

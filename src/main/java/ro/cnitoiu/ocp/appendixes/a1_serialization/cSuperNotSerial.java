package ro.cnitoiu.ocp.appendixes.a1_serialization;

import java.io.*;

/**
 * Created by cristian on 14.12.2015.
 */
public class cSuperNotSerial {

    public static void main(String [] args) {
        Dog3 d = new Dog3(35, "Fido");

        System.out.println("before: " + d.name + " " + d.weight);

        try {
            FileOutputStream fs = new FileOutputStream("testSer.ser");
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(d);
            os.close();
        }
        catch (Exception e) { e.printStackTrace(); }

        try {
            FileInputStream fis = new FileInputStream("testSer.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            d = (Dog3) ois.readObject();
            ois.close();
        }
        catch (Exception e) { e.printStackTrace(); }

        System.out.println("after: " + d.name + " " + d.weight);
    }

}

/*The instance variables from Dog will be serialized and deserialized correctly, but the inherited variables from
Animal will come back with their default/initially assigned values rather than the values they had at the time of serialization.
This is because the constructor for Animal will run as opposed to the one from Dog3 not being called*/
class Dog3 extends Animal implements Serializable{

    String name;

    Dog3(int w, String n) {
        weight = w;
        name = n;
    }
}

class Animal {
    int weight = 42;
}
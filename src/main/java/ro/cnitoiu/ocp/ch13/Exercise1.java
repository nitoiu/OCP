package ro.cnitoiu.ocp.ch13;

/**
 * Created by cristian on 17.11.2015.
 */
public class Exercise1 {

    public static void main(String[] args) {
        new Thread(new Task1()).start();
    }
}


class Task1 implements Runnable{
    int count;

    @Override
    public void run() {

        while(count < 100){
            ++count;

            try{
                Thread.sleep(1000);
            } catch (InterruptedException e){
                e.printStackTrace();
            }

            if(count % 10 == 0){
                System.out.println("random output string, count=" + count);
            }
        }

    }
}
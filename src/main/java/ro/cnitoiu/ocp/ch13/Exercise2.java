package ro.cnitoiu.ocp.ch13;

/**
 * Created by cristian.nitoiu on 18.11.2015.
 */
public class Exercise2 {

    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("A");

        for(int i = 0; i < 3; ++i){
            new MyThread(sb).start();
        }

    }
}

class MyThread extends Thread{

    private StringBuffer sb;

    MyThread(StringBuffer sb){
        this.sb = sb;
    }

    @Override
    public void run() {
        synchronized (sb){
            for(int i = 0; i < 100; ++i)
                System.out.print(sb);

            System.out.println("");
            sb.setCharAt(0, (char)(sb.charAt(0) + 1));
        }
    }
}

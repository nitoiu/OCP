package ro.cnitoiu.ocp.ch13;

/**
 * Created by cristian.nitoiu on 18.11.2015.
 */
public class aSynchronizeSyntax {

    public synchronized void doStuff1() {
        System.out.println("synchronized");
    }
    //is equivalent to this:
    public void doStuff2() {
        synchronized(this) {
            System.out.println("synchronized");
        }
    }

    private static int count;
    public static synchronized int getCount1() {
        return count;
    }
    //is equivalent to this:
    public static int getCount2() {
        synchronized(aSynchronizeSyntax.class) { //aSynchronizeSyntax.class is a class literal
            return count;
        }
    }
    public static void classMethod() throws ClassNotFoundException {
        Class cl = Class.forName("MyClass");
        synchronized (cl) {
        // do stuff
        }
    }


}

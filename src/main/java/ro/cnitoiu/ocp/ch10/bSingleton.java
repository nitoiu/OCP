package ro.cnitoiu.ocp.ch10;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by cristian on 28.10.2015.
 */
public enum bSingleton {
    INSTANCE;

    private Set<String> availableSeats;

    private bSingleton() {
        availableSeats = new HashSet<String>();
        availableSeats.add("1A");
        availableSeats.add("1B");
    }

    public boolean bookSeat(String seat) {
        return availableSeats.remove(seat);
    }

    public static void main(String[] args) {
        ticketAgentBooks("1A");
        ticketAgentBooks("1A");
    }

    private static void ticketAgentBooks(String seat) {
// we don't even need a method to get the instance
        bSingleton show = bSingleton.INSTANCE;
        System.out.println(show.bookSeat(seat));
    }
}

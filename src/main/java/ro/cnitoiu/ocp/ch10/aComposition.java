package ro.cnitoiu.ocp.ch10;

/**
 * Created by cristian.nitoiu on 27.10.2015.
 */
public class aComposition {
}

interface Box{
    void pack();
    void seal();
}

class GiftBox implements Box {
    public void pack() {
        System.out.println("pack box");
    }
    public void seal() {
        System.out.println("seal box");
    }
}

interface Mailer {
    void addPostage();
    void ship();
}

/* it isimportant for the composing class to both contain
and implement the same interface. */
class MailerBox implements Box, Mailer {
    private Box box;

    public MailerBox(Box box) {
        this.box = box;
    }

    /*method forwarding or method delegation*/
    public void pack() {
        box.pack();
    }
    public void seal() {
        box.seal();
    }
    public void addPostage() {
        System.out.println("affix stamps");
    }
    public void ship() {
        System.out.println("put in mailbox");
    }
}
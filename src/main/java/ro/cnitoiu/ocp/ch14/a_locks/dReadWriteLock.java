package ro.cnitoiu.ocp.ch14.a_locks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by cristian on 20.11.2015.
 */
public class dReadWriteLock {

    private List<Integer> integers = new ArrayList<>();
    private ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();

    public void add(Integer i) {
        Lock writeLock = rwl.writeLock();
        writeLock.lock(); // one at a time

        try {
            integers.add(i);
        } finally {
            writeLock.unlock();
        }
    }

    public int findMax() {

        Lock readLock = rwl.readLock();
        readLock.lock(); // many at once

        try {
            return Collections.max(integers);
        } finally {
            readLock.unlock();
        }
    }

}

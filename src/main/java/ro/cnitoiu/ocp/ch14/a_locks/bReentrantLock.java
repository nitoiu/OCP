package ro.cnitoiu.ocp.ch14.a_locks;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by cristian on 20.11.2015.
 */
public class bReentrantLock {

    public static void main(String[] args) {

        tryWithoutWaiting();
        avoidDeadLocking();
    }


    static void tryWithoutWaiting(){
        Lock lock = new ReentrantLock();

        boolean locked = lock.tryLock();
//        lock.lock(); //blocks until acquired. The alternataive to tryLock()
//        boolean locked = lock.tryLock(3, TimeUnit.SECONDS);  //throws InterruptedException;

        if (locked){
            try{
                //do some work
            } finally {
                lock.unlock();
            }
        }
    }

    static void avoidDeadLocking(){
        Lock l1 = new ReentrantLock();
        Lock l2 = new ReentrantLock();

        boolean aq1 = l1.tryLock();
        boolean aq2 = l2.tryLock();
        try{
            if (aq1 && aq2) {
                // work
            }
        } finally {
            if (aq2) l2.unlock(); // don't unlock if not locked
            if (aq1) l1.unlock();
        }
    }
}

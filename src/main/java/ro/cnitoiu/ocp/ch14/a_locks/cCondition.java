package ro.cnitoiu.ocp.ch14.a_locks;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by cristian on 20.11.2015.
 */
public class cCondition {

    public static void main(String[] args) {
        example();
    }

    static void example(){
        final Lock lock = new ReentrantLock();
        final Condition blockingPoolA = lock.newCondition();

        //this thread waits
        new Thread(){
            @Override
            public void run() {
                lock.lock();
                try {
                    blockingPoolA.await(); // "wait" here
                    // work
                } catch (InterruptedException ex) { // interrupted during await()
                } finally {
                    lock.unlock();
                }
            }
        }.start();

        //this thread signalsAll
        new Thread(){
            @Override
            public void run() {
                lock.lock();
                try {
                    blockingPoolA.signalAll(); // wake all awaiting threads
                    // work
                } finally {
                    lock.unlock();
                }
            }
        }.start();
    }
}

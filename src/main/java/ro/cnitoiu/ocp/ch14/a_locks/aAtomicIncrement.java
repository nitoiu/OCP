package ro.cnitoiu.ocp.ch14.a_locks;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by cristian on 20.11.2015.
 */
public class aAtomicIncrement {

    public static void main(String[] args){

        atomicCounter();
    }

    static void atomicCounter(){
        Counter c = new Counter();
        Thread t1 = new IncrementerThread(c);
        Thread t2 = new IncrementerThread(c);
        t1.start(); t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {}

        System.out.println(c.getValue());
    }
}

class Counter{
    private AtomicInteger count = new AtomicInteger();

    public void increment() {
        count.getAndIncrement(); //atomic operation
    }

    public int getValue() {
        return count.intValue();
    }
}
/* The atomic operation above:
1. The value stored in count is copied to a temporary variable.
2. The temporary variable is incremented.
3. Compare the value currently in count with the original value. If it is unchanged, then swap the old value for the new value.
Step 3 happens atomically. If step 3 finds that some other thread has already modified the value of count ,
then repeat steps 1–3 until we increment the field without interference.

 public final int getAndIncrement() {
        for (;;) {
            int current = get();
            int next = current + 1;
            if (compareAndSet(current, next))
                return current;
        }
    }
*/

class IncrementerThread extends Thread{

    Counter counter;

    IncrementerThread(Counter counter){
        this.counter = counter;
    }

    @Override
    public void run() {
        for(int i = 0; i < 10000; ++i){
            counter.increment();
        }
    }
}

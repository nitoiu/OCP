package ro.cnitoiu.ocp.ch14.c_executor;

import java.util.List;
import java.util.concurrent.*;

/**
 * Created by cristian on 21.11.2015.
 */
public class aSyntax {

    public static void main(String[] args) {

        //cached pool executor
        ExecutorService cached = Executors.newCachedThreadPool();

        //fixed pool executor
        ExecutorService fixed = Executors.newFixedThreadPool(4);

        //single pool executor
        ExecutorService single = Executors.newSingleThreadExecutor();


        //how to dynamically adjust the thread count of a pool
        ThreadPoolExecutor tpe = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);
        tpe.setCorePoolSize(8);
        tpe.setMaximumPoolSize(8);

        //Scheduled Thread Pool Syntax
        //----------------------------
        Runnable r = new ExampleRunnable();

        ScheduledExecutorService ftses = Executors.newScheduledThreadPool(4);
        ftses.schedule(r, 5, TimeUnit.SECONDS); //run once after 5 seconds
        ftses.scheduleAtFixedRate(r, 2, 5, TimeUnit.SECONDS); //begin after 2 seconds, run once every 5
        ftses.scheduleWithFixedDelay(r, 2, 5, TimeUnit.SECONDS); //begin after 2 seconds,
                                        // run again 5 seconds after the task was finished (the delay is 5 seconds)
        //all the methods return a Future object which can be used to check the task status asynchronously


        //Shutting down:
        cached.shutdown();

        try{
            boolean term = cached.awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException ex1){
            //did not wait the full 2 seconds
        } finally {
            if(!cached.isTerminated()) { //are all tasks done?
                List<Runnable> unfinished = cached.shutdownNow(); //a collection of unfinished tasks
            }
        }

    }
}

class ExampleRunnable implements Runnable{

    @Override
    public void run() {

    }
}

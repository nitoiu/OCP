package ro.cnitoiu.ocp.ch14.c_executor;

import java.util.concurrent.*;

/**
 * Created by cristian on 21.11.2015.
 */
public class bCallable {

    public static void main(String[] args) {

        Callable<Integer> callableTask = new MyCallable();

        ExecutorService executorService = Executors.newCachedThreadPool();
        Future<Integer> future = executorService.submit(callableTask); // finishes in the future

        try {
            Integer v = future.get(); // blocks until done
            System.out.println("Ran:" + v);
        } catch (InterruptedException | ExecutionException iex) {
//InterruptedException - Raised when the thread calling the Future 's get() method is interrupted before a result can be returned
//ExecutionException - Raised when an exception was thrown during the execution of the Callable 's call() method
            System.out.println("Failed");
        }
    }

}

class MyCallable implements Callable<Integer>{

    @Override
    public Integer call() throws Exception {
        // Obtain a random number from 1 to 10
        int count = ThreadLocalRandom.current().nextInt(1, 11);

        for(int i = 1; i <= count; i++) {
            System.out.println("Running..." + i);
        }
        return count;
    }
}
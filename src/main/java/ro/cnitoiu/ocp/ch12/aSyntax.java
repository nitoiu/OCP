package ro.cnitoiu.ocp.ch12;

/**
 * Created by cristian.nitoiu on 10.11.2015.
 */
public class aSyntax {
    public static void main(String[] args) {

        MyOuter mo = new MyOuter();// gotta get an instance!
//  Instantiating an inner class is the only scenario in
// which you'll invoke new on an instance
        MyOuter.MyInner inner = mo.new MyInner();
        inner.seeOuter();
    }
}

//Inner Classes
class MyOuter {
    private int x = 7;

    public void makeInner() {
        MyInner in = new MyInner();
        in.seeOuter();
    }

    class MyInner {
        public void seeOuter() {
            System.out.println("Outer x is " + x);
            System.out.println("Inner class ref is " + this);
            System.out.println("Outer class ref is " + MyOuter.this);
        }
    }
}

//(argument) anonymous inner class syntax:
class MyWonderfulClass {
    void go() {
        Bar b = new Bar();
        b.doStuff(new Foo() {
            public void foof() {
                System.out.println("foofy");
            } // end foof method
        }); // end inner class def, arg, and b.doStuff stmt.
    }
// end go()
}
// end class
interface Foo {
    void foof();
}
class Bar {
    void doStuff(Foo f) { }
}
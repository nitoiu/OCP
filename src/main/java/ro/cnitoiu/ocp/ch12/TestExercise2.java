package ro.cnitoiu.ocp.ch12;

/**
 * Created by cristian.nitoiu on 11.11.2015.
 */
public class TestExercise2 {

    public static void main(String[] args) {
        zBar.zoo();
    }

}

class Boo {
    Boo(String s) {
        System.out.println("boo");
    }
    Boo() { }
}
class zBar extends Boo {
    zBar() { }
    zBar(String s) {super(s);
        System.out.println("zBar");}
    static void zoo() {
//        Boo f = new Bar() { };
//        Boo f = new Boo() {String s; };
        Boo f = new zBar("s") { }; //Bar f = new Boo(String s) { }; in the test
    }
}
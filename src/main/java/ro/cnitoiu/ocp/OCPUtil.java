package ro.cnitoiu.ocp;

/**
 * Created by cristian on 21.10.2015.
 */
public class OCPUtil {

    public static void delimit(int type){

        switch (type){
            case 1:
                System.out.println(String.format("%20s", "- - -"));
                break;

            default:
                delimit(DelimiterType.Lines);
        }

    }

    public static void delimit(){
        delimit(999); //should trigger the default
    }

    public static class DelimiterType {
        public static int Lines = 1;
    }
}

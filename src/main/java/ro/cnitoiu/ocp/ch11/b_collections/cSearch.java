package ro.cnitoiu.ocp.ch11.b_collections;

import ro.cnitoiu.ocp.OCPUtil;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by cristian.nitoiu on 03.11.2015.
 */
public class cSearch {

    /*
    -   binarySearch()
    -   positive for successful search, negative for insertion point ( - (insertion point) -1 )
    -   collection/array must be sorted
    -   comparable/comparator - the same should be used for both sorting and searching
     */

    public static void main(String[] args) {

        sortExample();

    }


    private static void sortExample(){

        String [] sa = {"one", "two", "three", "four"};
        Arrays.sort(sa);

        for(String s : sa)
            System.out.print(s + " ");

        System.out.println("\none = " + Arrays.binarySearch(sa,"one"));

        OCPUtil.delimit();
        System.out.println("now reverse sort");

        ReSortComparator rs = new ReSortComparator();
        Arrays.sort(sa,rs);

        for(String s : sa)
            System.out.print(s + " ");
        System.out.println("\none = " + Arrays.binarySearch(sa,"one"));
        System.out.println("one = " + Arrays.binarySearch(sa,"one",rs));
    }

    static class ReSortComparator implements Comparator<String> {
        public int compare(String a, String b) {
            return b.compareTo(a);
        }
    }

}

package ro.cnitoiu.ocp.ch11.b_collections;

/**
 * Created by cristian.nitoiu on 03.11.2015.
 */
public class bBoxing {

    public static void main(String[] args) {

        immutableExample();
        complexExample();
        nullPointerException();
    }

    private static void immutableExample(){
        Integer y = 567;
        Integer x = y;

        System.out.println(y==x);

        y++; //y actually gets another ref.

        System.out.println(x + " " + y);
        System.out.println(y==x);
    }

    private static void complexExample(){
        bBoxing u = new bBoxing();
        u.go(5);
    }

    boolean go(Integer i) { // boxes the int it was passed
        Boolean ifSo = true; // boxes the literal
        Short s = 300; // boxes the primitive

        if(ifSo) { // unboxing
            System.out.println(++s); // unboxes, increments, reboxes
        }
        return !ifSo; // unboxes, returns the inverse

    }

    private static void nullPointerException(){

        class Boxing2 {
            Integer x;

            void doStuff(int z) {
                int z2 = 5;
                System.out.println(z2 + z);
            }
        }

        Boxing2 b = new Boxing2();
        b.doStuff(b.x);
    }
}

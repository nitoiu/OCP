package ro.cnitoiu.ocp.ch11.b_collections;

import java.util.TreeSet;

/**
 * Created by cristian on 04.11.2015.
 */
public class fNavigating {

    public static void main(String[] args) {
        ferryTreeSetExample();
    }

    private static void ferryTreeSetExample(){
//        1. The last ferry that leaves before 4 pm (1600 hours)
//        2. The first ferry that leaves after 8 pm (2000 hours)

        TreeSet<Integer> times = new TreeSet<>();

        // add some departure times
        times.add(1205);
        times.add(1505);
        times.add(1545);
        times.add(1830);
        times.add(2010);
        times.add(2100);

        // Java 5 version
        TreeSet<Integer> subset = (TreeSet)times.headSet(1600);
        System.out.println("J5 - last before 4pm is: " + subset.last());

        TreeSet<Integer> sub2 = (TreeSet)times.tailSet(2000);
        System.out.println("J5 - first after 8pm is: " + sub2.first());

        // Java 6 version using the new lower() and higher() methods
        System.out.println("J6 - last before 4pm is: " + times.lower(1600));
        System.out.println("J6 - first after 8pm is: " + times.higher(2000));

        //NavigableSet exam methods:
        //lower() , floor() - less than or equal , higher() , and ceiling() - higher than or equal

        //TreeSet/TreeMap polling:
        // pollFirst() returns and removes the first entry in the set.. similar to map's pollFirstEntry()
        // pollLast() returns and removes the last entry..  pollLastEntry()

        //TreeSet.descendingSet(), TreeMap.descendingMap() \
        //  - returns corresponding collection in the reverse order
    }
}

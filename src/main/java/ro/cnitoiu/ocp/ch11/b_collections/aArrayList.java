package ro.cnitoiu.ocp.ch11.b_collections;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cristian.nitoiu on 03.11.2015.
 */
public class aArrayList {

    /* Advantages:
    - it can grow dynamically
    - it provides more powerful isertion and search mechanisms than arrays.
     */

    public static void main(String[] args) {

        List<String> test = new ArrayList<>(); //polymorphically
        String s = "hi";

        test.add("string");
        test.add(s);
        test.add(s+s);

        System.out.println(test.size());
        System.out.println(test.contains(42));
        System.out.println(test.contains("hihi"));

        test.remove("hi");
        System.out.println(test.size());

    }
}

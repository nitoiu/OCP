package ro.cnitoiu.ocp.ch11.b_collections;

import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by cristian on 04.11.2015.
 */
public class gBackedCollections {

    public static void main(String[] args) {

        example();
    }

    private static void example(){
        TreeMap<String, String> map = new TreeMap(){{
            put("a", "ant");
            put("d", "dog");
            put("h", "horse");
        }};

        //Backed Collection
        SortedMap<String, String> submap = map.subMap("b", "g");
        System.out.println(map + "\n" + submap);

        //add to both collections
        map.put("b", "bat");
        submap.put("f", "fish");
        map.put("r", "raccoon");
        // submap.put("p", "pig");  //out of range

        System.out.println(map + "\n" + submap);

        /*
        TreeSet — headSet() , subSet() , and tailSet()
        TreeMap — headMap() , subMap() , and tailMap()
        subSet() and subMap() get 2 arguments start and end, the others only one.
         */

    }
}

package ro.cnitoiu.ocp.ch11.b_collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by cristian.nitoiu on 03.11.2015.
 */
public class dConvertingToArrays {

    public static void main(String[] args) {

        /*Arrays.asList():
        Changes to the returned list'write through' to the array.
        */
        asListExample();

        toArrayExample();

    }


    private static void asListExample(){
        String[] sa = {"one", "two", "three", "four"};
        List sList = Arrays.asList(sa);// make a List

        System.out.println("size " + sList.size());
        System.out.println("idx2 " + sList.get(2));

        sList.set(3,"six");// change List
        sa[1] = "five";// change array

        for(String s : sa)
            System.out.print(s + " ");

        System.out.println("\nsList.get(1) " + sList.get(1));
    }

    private static void toArrayExample(){

        List<Integer> iL = new ArrayList(){{
            for(int x=0; x<3; x++) add(x);
        }};

        Object[] oa = iL.toArray();// create an Object array

        Integer[] ia2 = new Integer[3];
        ia2 = iL.toArray(ia2); //uses ia2 as destination array
    }
}

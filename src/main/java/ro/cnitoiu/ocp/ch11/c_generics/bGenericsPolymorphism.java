package ro.cnitoiu.ocp.ch11.c_generics;

import javax.swing.*;

/**
 * Created by cristian.nitoiu on 04.11.2015.
 */
public class bGenericsPolymorphism {

    public static void main(String[] args) {

        arraysGenerics();

    }

    private static void arraysGenerics(){
        /*
        This code is valid for arrays, not for collections.
        Collections need to have the same parameter type
         */

        Parent[] myArray = new Child[3];
        Object[] myOtherArray = new JButton[3];
    }

/*    Invalid code:
    List<Object> myList = new ArrayList<JButton>();
    List<Number> numbers = new ArrayList<Integer>();
 */
}

class Parent{}
class Child extends Parent { }


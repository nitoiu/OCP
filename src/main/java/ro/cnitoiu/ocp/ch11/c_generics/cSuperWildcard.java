package ro.cnitoiu.ocp.ch11.c_generics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cristian.nitoiu on 05.11.2015.
 */
public class cSuperWildcard {

    public static void main(String[] args) {

        List<Animal> animals = new ArrayList<Animal>();
        animals.add(new Dog());
        animals.add(new Dog());

        cSuperWildcard doc = new cSuperWildcard();
        doc.addAnimal(animals);

    }

    /*
    a collection declared as any
    supertype of Dog will be able to accept a Dog as an element
     */
    private void addAnimal(List<? super Dog> animals){
        animals.add(new Dog());
    }
}

class Dog extends Animal{}
class Animal{}

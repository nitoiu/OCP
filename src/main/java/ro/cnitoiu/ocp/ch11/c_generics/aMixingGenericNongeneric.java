package ro.cnitoiu.ocp.ch11.c_generics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by cristian.nitoiu on 04.11.2015.
 */
public class aMixingGenericNongeneric {

    public static void main(String[] args) {

        exampleThatWorks();
        anInconvenience();
    }

    private static void exampleThatWorks(){
        List<Integer> myList = new ArrayList<>();// type safe collection
        myList.add(4);
        myList.add(6);

        Adder adder = new Adder();
        int total = adder.addAll(myList); // pass it to an untyped argument
        System.out.println(total);
    }

    private static void anInconvenience(){
        List<Integer> myList = new ArrayList<>();
        myList.add(4);
        myList.add(6);

        Inserter in = new Inserter();
        in.insert(myList);
    }
}

class Adder {
    int addAll(List list) {
        // method with a non-generic List argument,
        // but assumes (with no guarantee) that it will be Integers
        Iterator it = list.iterator();
        int total = 0;
        while (it.hasNext()) {
            int i = ((Integer)it.next()).intValue();
            total += i;
        }
        return total;
    }
}

class Inserter {
    // method with a non-generic List argument
    void insert(List list) {
        list.add(new Integer(42)); // adds to the incoming list - warning
        list.add(new String("42")); // also works
/*someone just stuffed a String into a supposedly type-safe ArrayList of type <Integer> .
The actof adding a String to an <Integer> list won't fail at runtime until you try to treat
    that String -you-think-is-an- Integer as an Integer.
    */
    }
}
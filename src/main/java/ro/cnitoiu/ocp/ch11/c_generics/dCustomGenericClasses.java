package ro.cnitoiu.ocp.ch11.c_generics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cristian.nitoiu on 05.11.2015.
 */
public class dCustomGenericClasses {

    public static void main(String[] args) {
        Car c1 = new Car();
        Car c2 = new Car();

        List<Car> carList = new ArrayList<>();
        carList.add(c1);
        carList.add(c2);

        RentalGeneric<Car> carRental = new RentalGeneric<>(2, carList);

        Car carToRent = carRental.getRental();
        carRental.returnRental(carToRent);

// can we stick something else in the original carList? NO
//        carList.add(new Cat("Fluffy"));


        //more generics syntax:
        UseTwo<Exception, Car> u2 = new UseTwo<>(new Exception(), new Car());
        Car c = u2.getX();
        Exception e = u2.getT();
    }
}

class RentalGeneric<T> {
    private List<T> rentalPool;
    private int maxNum;

    public RentalGeneric(int maxNum, List<T> rentalPool) {
        this.maxNum = maxNum;
        this.rentalPool = rentalPool;
    }

    public T getRental() {
        return rentalPool.get(0);
    }

    public void returnRental(T returnedThing) {
        rentalPool.add(returnedThing);
    }
}

class Car {}
class Cat {public Cat(String s){super();}}


class GenericsSyntax<T> {
    T anInstance;
    T [] anArrayOfTs;

    GenericsSyntax(T anInstance) {
        this.anInstance = anInstance;
    }

    T getT() {
        return anInstance;
    }

    /*method syntax with generics
    You MUST declare the type like that unless the type is specified for the class.
     */
    public static <Y extends Number> void makeArrayList(Y y){

        List<Y> list = new ArrayList<>();
        list.add(y);
    }
}

class UseTwo<T, X extends Car> {
    T one;
    X two;

    UseTwo(T one, X two) {
        this.one = one;
        this.two = two;
    }

    T getT() {
        return one;
    }

    X getX() {
        return two;
    }
}

class X { public <X> X(X x) { } }
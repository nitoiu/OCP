package ro.cnitoiu.ocp.ch11.tests;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cristian.nitoiu on 02.11.2015.
 */
public class ListTests {
    public static void main(String[] args) {

        testListInsertion();
    }

    private static void testListInsertion(){

        List<Integer> list = new ArrayList(){{
            add(0);
            add(1);
            add(2);
        }};

        list.add(1, 11);

        System.out.println(list);
    }
}

package ro.cnitoiu.ocp.ch11.tests;

import java.util.*;

/**
 * Created by cristian.nitoiu on 06.11.2015.
 */
public class SelfTest {

    public static void main(String[] args) {
        question01();
        question09();
        question13();
        question15();
    }

    private static void question01(){
        List<List<Integer>> table = new ArrayList<List<Integer>>();
        //Apparently the answer from the book is wrong. It's B not A.
    }

    private static void question09(){
        PriorityQueue<String> pq = new PriorityQueue<String>();
        pq.add("2");
        pq.add("4");
        System.out.print(pq.peek() + " ");
        pq.offer("1");
        pq.add("3");
        pq.remove("1");
        System.out.print(pq.poll() + " ");
        if(pq.remove("2")) System.out.print(pq.poll() + " ");
        System.out.println(pq.poll() + " " + pq.peek());
    }

    private static void question13(){
        TreeMap<String, String> myMap = new TreeMap<>();
        myMap.put("a", "apple");
        myMap.put("d", "date");
        myMap.put("f", "fig");
        myMap.put("p", "pear");

        System.out.println("1st after mango: " + myMap.higherKey("f")); // sop 1
        System.out.println("1st after mango: " + myMap.ceilingKey("f")); // sop 2
        System.out.println("1st after mango: " + myMap.floorKey("f")); // sop 3

        SortedMap<String, String> sub = myMap.tailMap("f");
        System.out.println("1st after mango: " + sub.firstKey()); // sop 4
    }

    private static void question15(){
        TreeSet<Integer> i = new TreeSet(){{
            add(new Dog(1));
            add(new Dog(2));
            add(new Dog(1));
        }};

        TreeSet<Dog> d = new TreeSet(){{
            add(1);
            add(2);
            add(1);
        }};

        System.out.println(d.size() + " " + i.size());

    }
}
class Dog { int size; Dog(int s) { size = s; } }
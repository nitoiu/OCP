-> If you don't override a class's equals() method, you won't be able to use those objects as a key in a hashtable and you
    probably won't get accurate Sets such that there are no conceptual duplicates.
  The equals() method in class Object uses only the == operator for comparisons,
    so unless you override equals() , two objects are considered equal only if the two
    references refer to the same object.

-> The equals() contract:
    - reflexive - x.equals(x) returns true
    - symmetric - x.equals(y) <=> y.equals(x)
    - transitive - if x.equals(y) and y.equals(z) are true then x.equals(z) must also return true
    - consistent - if x.equals(y) returns true, it must consistently do so.
    - x.equals(null) should return false

-> equals() and hashCode() are bound together by a joint contract that specifies if two objects are
    considered equal using the equals() method, then they must have identical hashcode values.
    (rule of thumb: if you override equals() , override hashCode() as well)

-> Hashing retrieval has 2 steps:
    1. Find the right bucket (using hashCode() ).
    2. Search the bucket for the right element (using equals() ).

-> hashCode() implementation should use the same instance variables used for equals()

-> The hashCode() contract:
    - hashCode() must always return the same integer for the same state,
        but not necessarily the same on different executions of the program.
    - 2 equal objects (using equals()) must return the same integer for hashCode()
    - 2 unequal objects can have the same hashCode.

-> it's legal but not correct to use transient fields in hashCode() and equals()



General Info about collections:

-> sorting is a specific type of ordering
When a collection is ordered, it means you can iterate through the collection in a specific (not random) order.

-> The one thing that List has that non-lists don't is a set of methods related to the index

-> A Vector is basically the same as an ArrayList , but Vector methods are synchronized for thread safety.

-> Only ArrayList and Vector implement RandomAccess marker interface.

-> LinkedList: double linked, Queue(since 1.5)

-> LinkedHashSet: doubly linked, maintains insertion order.

-> TreeSet and TreeMap are the 2 sorted collections, implements NavigableSet since 1.6

-> Hashtable is the synchronized counterpart to HashMap
-> HashMap lets you have null values as well as one null key, a
   Hashtable doesn't let you have anything that's null .

-> LinkedList is used for basic queue implementations while PriorityQueue is also sorted

-> if you don't override hashCode() , every object will have a unique hashcode.
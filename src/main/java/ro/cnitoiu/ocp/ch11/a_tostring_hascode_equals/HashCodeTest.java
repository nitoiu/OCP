package ro.cnitoiu.ocp.ch11.a_tostring_hascode_equals;

/**
 * Created by cristian.nitoiu on 29.10.2015.
 */
public class HashCodeTest {

    @Override
    public int hashCode() {
        return 5;
    }

    public static void main(String[] args) {
        //prints: hashCodeTest@5
        System.out.println(new HashCodeTest());
    }
}
